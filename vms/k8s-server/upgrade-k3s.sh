#!/usr/bin/env bash

#curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC='--disable=traefik --disable servicelb' K3S_KUBECONFIG_MODE="644" INSTALL_K3S_CHANNEL='v1.30' sh -
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable=traefik --disable servicelb" K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION=$(cat k3s.version) sh -

# https://github.com/k3s-io/k3s/issues/2068#issuecomment-1374672584
# Alternative solution based on symlinks: https://mrkandreev.name/snippets/how_to_move_k3s_data_to_another_location
