#!/usr/bin/env bash

set -euo pipefail

source /opt/minio/version.env
export VERSIONS_FOLDER=/opt/minio/versions
mkdir -p $VERSIONS_FOLDER

echo "Download $VERSION"
curl -Lqs https://dl.min.io/server/minio/release/linux-amd64/minio.$VERSION -o $VERSIONS_FOLDER/minio.$VERSION
chmod +x $VERSIONS_FOLDER/minio.$VERSION

echo "Check binary is valid"
$VERSIONS_FOLDER/minio.$VERSION --version

echo "Stop previous minio instance"
sudo systemctl stop minio

echo "Link production binary"
ln -fs $VERSIONS_FOLDER/minio.$VERSION /opt/minio/minio

echo "Start new production instance"
sudo systemctl start minio
