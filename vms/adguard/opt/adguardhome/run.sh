#!/usr/bin/env bash

source /opt/adguardhome/version.env
/usr/bin/docker run --rm --name $1 \
   -v /opt/adguardhome/work:/opt/adguardhome/work \
   -v /opt/adguardhome/conf:/opt/adguardhome/conf \
   -v /opt/adguardhome/certs:/opt/adguardhome/certs \
   --user 1001:1001 \
   --cap-add=NET_ADMIN \
   --network host \
   adguard/adguardhome:$VERSION