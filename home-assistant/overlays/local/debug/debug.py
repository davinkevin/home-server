import time
import os

from sqlalchemy import (
    Select,
    create_engine,
    func,
    lateral,
    select,
    label,
    true,
    union_all,
)
from sqlalchemy import (
    Select,
    create_engine,
    func,
    lateral,
    select,
    label,
    true,
    and_,
    or_,
    distinct,
)

from homeassistant.components.recorder.db_schema import (
    States,
    StatesMeta,
    EventTypes,
    StateAttributes,
)

from homeassistant.components.recorder.db_schema import States, Events, EventData

engine = create_engine(os.environ['RECORDER_URI'])
# engine = create_engine("mysql://x:x@/x")
# engine = create_engine("sqlite:///:memory:")


def _get_start_time_state_for_entities_stmt(
    attributes_ids: list[int],
) -> Select:
    return (
        select(EventData.data_id)
        .select_from(EventData)
        .join(
            Events,
            and_(
                Events.data_id == EventData.data_id,
                Events.time_fired_ts
                == select(Events.time_fired_ts)
                .where(Events.data_id == EventData.data_id)
                .limit(1)
                .scalar_subquery()
                .correlate(EventData),
            ),
        )
        .where(EventData.data_id.in_(attributes_ids))
    )


stmt = _get_start_time_state_for_entities_stmt(
    [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        23,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
        32,
        33,
        34,
        35,
        36,
        37,
        40,
        41,
        42,
        43,
        44,
        45,
        46,
        47,
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
    ],
)
# print('stmt')
# print(stmt)
print("stmt.compile")
compiled = stmt.compile(dialect=engine.dialect, compile_kwargs={"literal_binds": True})
print(compiled)
# print('stmt.compile.params')
# print(compiled.params)
# exit(0)
with engine.connect() as conn:
    result = conn.execute(stmt)
    print("result")
    print(result)
    for row in result:
        print(row)