alter table public.states
    drop constraint states_old_state_id_fkey,
    add foreign key (old_state_id) references public.states
        on delete set null
        deferrable initially immediate;

