---
# Source: mastodon/templates/pdb-sidekiq.yaml
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: mastodon-sidekiq-all-queues
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: sidekiq-all-queues
    app.kubernetes.io/part-of: rails
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: mastodon
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/component: sidekiq-all-queues
      app.kubernetes.io/part-of: rails
  maxUnavailable: 1
---
# Source: mastodon/templates/pdb-streaming.yaml
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: mastodon-streaming
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: streaming
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: mastodon
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/component: streaming
  maxUnavailable: 1
---
# Source: mastodon/templates/pdb-web.yaml
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: mastodon-web
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: web
    app.kubernetes.io/part-of: rails
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: mastodon
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/component: web
      app.kubernetes.io/part-of: rails
  maxUnavailable: 1
---
# Source: mastodon/charts/redis/templates/master/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
automountServiceAccountToken: false
metadata:
  name: mastodon-redis-master
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
---
# Source: mastodon/charts/redis/templates/replicas/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
automountServiceAccountToken: false
metadata:
  name: mastodon-redis-replica
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
---
# Source: mastodon/templates/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: mastodon
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
---
# Source: mastodon/charts/redis/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mastodon-redis-configuration
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
data:
  redis.conf: |-
    # User-supplied common configuration:
    # Enable AOF https://redis.io/topics/persistence#append-only-file
    appendonly yes
    # Disable RDB persistence, AOF persistence already enabled.
    save ""
    # End of common configuration
  master.conf: |-
    dir /data
    # User-supplied master configuration:
    rename-command FLUSHDB ""
    rename-command FLUSHALL ""
    # End of master configuration
  replica.conf: |-
    dir /data
    # User-supplied replica configuration:
    rename-command FLUSHDB ""
    rename-command FLUSHALL ""
    # End of replica configuration
---
# Source: mastodon/charts/redis/templates/health-configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mastodon-redis-health
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
data:
  ping_readiness_local.sh: |-
    #!/bin/bash

    [[ -f $REDIS_PASSWORD_FILE ]] && export REDIS_PASSWORD="$(< "${REDIS_PASSWORD_FILE}")"
    [[ -n "$REDIS_PASSWORD" ]] && export REDISCLI_AUTH="$REDIS_PASSWORD"
    response=$(
      timeout -s 15 $1 \
      redis-cli \
        -h localhost \
        -p $REDIS_PORT \
        ping
    )
    if [ "$?" -eq "124" ]; then
      echo "Timed out"
      exit 1
    fi
    if [ "$response" != "PONG" ]; then
      echo "$response"
      exit 1
    fi
  ping_liveness_local.sh: |-
    #!/bin/bash

    [[ -f $REDIS_PASSWORD_FILE ]] && export REDIS_PASSWORD="$(< "${REDIS_PASSWORD_FILE}")"
    [[ -n "$REDIS_PASSWORD" ]] && export REDISCLI_AUTH="$REDIS_PASSWORD"
    response=$(
      timeout -s 15 $1 \
      redis-cli \
        -h localhost \
        -p $REDIS_PORT \
        ping
    )
    if [ "$?" -eq "124" ]; then
      echo "Timed out"
      exit 1
    fi
    responseFirstWord=$(echo $response | head -n1 | awk '{print $1;}')
    if [ "$response" != "PONG" ] && [ "$responseFirstWord" != "LOADING" ] && [ "$responseFirstWord" != "MASTERDOWN" ]; then
      echo "$response"
      exit 1
    fi
  ping_readiness_master.sh: |-
    #!/bin/bash

    [[ -f $REDIS_MASTER_PASSWORD_FILE ]] && export REDIS_MASTER_PASSWORD="$(< "${REDIS_MASTER_PASSWORD_FILE}")"
    [[ -n "$REDIS_MASTER_PASSWORD" ]] && export REDISCLI_AUTH="$REDIS_MASTER_PASSWORD"
    response=$(
      timeout -s 15 $1 \
      redis-cli \
        -h $REDIS_MASTER_HOST \
        -p $REDIS_MASTER_PORT_NUMBER \
        ping
    )
    if [ "$?" -eq "124" ]; then
      echo "Timed out"
      exit 1
    fi
    if [ "$response" != "PONG" ]; then
      echo "$response"
      exit 1
    fi
  ping_liveness_master.sh: |-
    #!/bin/bash

    [[ -f $REDIS_MASTER_PASSWORD_FILE ]] && export REDIS_MASTER_PASSWORD="$(< "${REDIS_MASTER_PASSWORD_FILE}")"
    [[ -n "$REDIS_MASTER_PASSWORD" ]] && export REDISCLI_AUTH="$REDIS_MASTER_PASSWORD"
    response=$(
      timeout -s 15 $1 \
      redis-cli \
        -h $REDIS_MASTER_HOST \
        -p $REDIS_MASTER_PORT_NUMBER \
        ping
    )
    if [ "$?" -eq "124" ]; then
      echo "Timed out"
      exit 1
    fi
    responseFirstWord=$(echo $response | head -n1 | awk '{print $1;}')
    if [ "$response" != "PONG" ] && [ "$responseFirstWord" != "LOADING" ]; then
      echo "$response"
      exit 1
    fi
  ping_readiness_local_and_master.sh: |-
    script_dir="$(dirname "$0")"
    exit_status=0
    "$script_dir/ping_readiness_local.sh" $1 || exit_status=$?
    "$script_dir/ping_readiness_master.sh" $1 || exit_status=$?
    exit $exit_status
  ping_liveness_local_and_master.sh: |-
    script_dir="$(dirname "$0")"
    exit_status=0
    "$script_dir/ping_liveness_local.sh" $1 || exit_status=$?
    "$script_dir/ping_liveness_master.sh" $1 || exit_status=$?
    exit $exit_status
---
# Source: mastodon/charts/redis/templates/scripts-configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mastodon-redis-scripts
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
data:
  start-master.sh: |
    #!/bin/bash

    [[ -f $REDIS_PASSWORD_FILE ]] && export REDIS_PASSWORD="$(< "${REDIS_PASSWORD_FILE}")"
    if [[ -f /opt/bitnami/redis/mounted-etc/master.conf ]];then
        cp /opt/bitnami/redis/mounted-etc/master.conf /opt/bitnami/redis/etc/master.conf
    fi
    if [[ -f /opt/bitnami/redis/mounted-etc/redis.conf ]];then
        cp /opt/bitnami/redis/mounted-etc/redis.conf /opt/bitnami/redis/etc/redis.conf
    fi
    ARGS=("--port" "${REDIS_PORT}")
    ARGS+=("--requirepass" "${REDIS_PASSWORD}")
    ARGS+=("--masterauth" "${REDIS_PASSWORD}")
    ARGS+=("--include" "/opt/bitnami/redis/etc/redis.conf")
    ARGS+=("--include" "/opt/bitnami/redis/etc/master.conf")
    exec redis-server "${ARGS[@]}"
  start-replica.sh: |
    #!/bin/bash

    get_port() {
        hostname="$1"
        type="$2"

        port_var=$(echo "${hostname^^}_SERVICE_PORT_$type" | sed "s/-/_/g")
        port=${!port_var}

        if [ -z "$port" ]; then
            case $type in
                "SENTINEL")
                    echo 26379
                    ;;
                "REDIS")
                    echo 6379
                    ;;
            esac
        else
            echo $port
        fi
    }

    get_full_hostname() {
        hostname="$1"
        full_hostname="${hostname}.${HEADLESS_SERVICE}"
        echo "${full_hostname}"
    }

    REDISPORT=$(get_port "$HOSTNAME" "REDIS")
    HEADLESS_SERVICE="mastodon-redis-headless.mastodon.svc.cluster.local"

    [[ -f $REDIS_PASSWORD_FILE ]] && export REDIS_PASSWORD="$(< "${REDIS_PASSWORD_FILE}")"
    [[ -f $REDIS_MASTER_PASSWORD_FILE ]] && export REDIS_MASTER_PASSWORD="$(< "${REDIS_MASTER_PASSWORD_FILE}")"
    if [[ -f /opt/bitnami/redis/mounted-etc/replica.conf ]];then
        cp /opt/bitnami/redis/mounted-etc/replica.conf /opt/bitnami/redis/etc/replica.conf
    fi
    if [[ -f /opt/bitnami/redis/mounted-etc/redis.conf ]];then
        cp /opt/bitnami/redis/mounted-etc/redis.conf /opt/bitnami/redis/etc/redis.conf
    fi

    echo "" >> /opt/bitnami/redis/etc/replica.conf
    echo "replica-announce-port $REDISPORT" >> /opt/bitnami/redis/etc/replica.conf
    echo "replica-announce-ip $(get_full_hostname "$HOSTNAME")" >> /opt/bitnami/redis/etc/replica.conf
    ARGS=("--port" "${REDIS_PORT}")
    ARGS+=("--replicaof" "${REDIS_MASTER_HOST}" "${REDIS_MASTER_PORT_NUMBER}")
    ARGS+=("--requirepass" "${REDIS_PASSWORD}")
    ARGS+=("--masterauth" "${REDIS_MASTER_PASSWORD}")
    ARGS+=("--include" "/opt/bitnami/redis/etc/redis.conf")
    ARGS+=("--include" "/opt/bitnami/redis/etc/replica.conf")
    exec redis-server "${ARGS[@]}"
---
# Source: mastodon/templates/configmap-env.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mastodon-env
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
data:
  DB_HOST: database-v17-rw
  DB_PORT: "5432"
  DB_NAME: mastodon_production
  DB_POOL: "25"
  DB_USER: mastodon
  PREPARED_STATEMENTS: "true"
  DEFAULT_LOCALE: en
  LOCAL_DOMAIN: davinkevin.fr
  WEB_DOMAIN: mastodon.davinkevin.fr
  # https://devcenter.heroku.com/articles/tuning-glibc-memory-behavior
  MALLOC_ARENA_MAX: "2"
  NODE_ENV: "production"
  RAILS_ENV: "production"
  REDIS_HOST: mastodon-redis-master
  REDIS_PORT: "6379"
  SMTP_AUTH_METHOD: plain
  SMTP_CA_FILE: /etc/ssl/certs/ca-certificates.crt
  SMTP_DELIVERY_METHOD: smtp
  SMTP_DOMAIN: mastodon.davinkevin.fr
  SMTP_ENABLE_STARTTLS: "auto"
  SMTP_FROM_ADDRESS: noreply@mastodon.davinkevin.fr
  SMTP_OPENSSL_VERIFY_MODE: peer
  SMTP_PORT: "587"
  SMTP_SERVER: smtp.gmail.com
  STREAMING_CLUSTER_NUM: "1"
---
# Source: mastodon/templates/pvc-assets.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mastodon-assets
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
  storageClassName: internal-ssd
---
# Source: mastodon/templates/pvc-system.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mastodon-system
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Gi
  storageClassName: internal-ssd
---
# Source: mastodon/charts/redis/templates/headless-svc.yaml
apiVersion: v1
kind: Service
metadata:
  name: mastodon-redis-headless
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
  annotations:
    
spec:
  type: ClusterIP
  clusterIP: None
  ports:
    - name: tcp-redis
      port: 6379
      targetPort: redis
  selector:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/name: redis
---
# Source: mastodon/charts/redis/templates/master/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: mastodon-redis-master
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
    app.kubernetes.io/component: master
spec:
  type: ClusterIP
  internalTrafficPolicy: Cluster
  sessionAffinity: None
  ports:
    - name: tcp-redis
      port: 6379
      targetPort: redis
      nodePort: null
  selector:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/name: redis
    app.kubernetes.io/component: master
---
# Source: mastodon/charts/redis/templates/replicas/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: mastodon-redis-replicas
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
    app.kubernetes.io/component: replica
spec:
  type: ClusterIP
  internalTrafficPolicy: Cluster
  sessionAffinity: None
  ports:
    - name: tcp-redis
      port: 6379
      targetPort: redis
      nodePort: null
  selector:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/name: redis
    app.kubernetes.io/component: replica
---
# Source: mastodon/templates/service-streaming.yaml
apiVersion: v1
kind: Service
metadata:
  name: mastodon-streaming
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
    - port: 4000
      targetPort: streaming
      protocol: TCP
      name: streaming
  selector:
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/component: streaming
---
# Source: mastodon/templates/service-web.yaml
apiVersion: v1
kind: Service
metadata:
  name: mastodon-web
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
    - port: 3000
      targetPort: http
      protocol: TCP
      name: http
  selector:
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/component: web
---
# Source: mastodon/templates/deployment-sidekiq.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mastodon-sidekiq-all-queues
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: sidekiq-all-queues
    app.kubernetes.io/part-of: rails
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: mastodon
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/component: sidekiq-all-queues
      app.kubernetes.io/part-of: rails
  template:
    metadata:
      annotations:
        # roll the pods to pick up any db migrations or other changes
        checksum/config-secrets: "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b"
        checksum/config-configmap: "63b8ff4261fd1f4f3b79b90dbfa49e7f74dc30b0b06ddd2f8bf29c0cf9462f63"
      labels:
        app.kubernetes.io/name: mastodon
        app.kubernetes.io/instance: mastodon
        app.kubernetes.io/component: sidekiq-all-queues
        app.kubernetes.io/part-of: rails
    spec:
      serviceAccountName: mastodon
      securityContext:
        fsGroup: 991
        runAsGroup: 991
        runAsNonRoot: true
        runAsUser: 991
        seccompProfile:
          type: RuntimeDefault
      volumes:
        
        - name: assets
          persistentVolumeClaim:
            claimName: mastodon-assets
        - name: system
          persistentVolumeClaim:
            claimName: mastodon-system
        - name: tmp
          emptyDir:
            medium: Memory
      containers:
        - name: mastodon
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            readOnlyRootFilesystem: true
          image: "ghcr.io/mastodon/mastodon:v4.3.4"
          imagePullPolicy: IfNotPresent
          command:
            - bundle
            - exec
            - sidekiq
            - -c
            - "25"
            - -q
            - "default,8"
            - -q
            - "push,6"
            - -q
            - "ingress,4"
            - -q
            - "mailers,2"
            - -q
            - "pull,1"
            - -q
            - "scheduler,1"
          envFrom:
            - configMapRef:
                name: mastodon-env
            - secretRef:
                name: mastodon
          env:
            - name: "DB_PASS"
              valueFrom:
                secretKeyRef:
                  name: database
                  key: password
            - name: "REDIS_PASSWORD"
              valueFrom:
                secretKeyRef:
                  name: redis
                  key: redis-password
            - name: "SMTP_LOGIN"
              valueFrom:
                secretKeyRef:
                  name: email
                  key: login
                  optional: true
            - name: "SMTP_PASSWORD"
              valueFrom:
                secretKeyRef:
                  name: email
                  key: password
          startupProbe:
            exec:
              command:
                - test
                - -e
                - /opt/mastodon/tmp/sidekiq_process_has_started_and_will_begin_processing_jobs
            failureThreshold: 30
            periodSeconds: 5
          volumeMounts:
            
            - name: assets
              mountPath: /opt/mastodon/public/assets
            - name: system
              mountPath: /opt/mastodon/public/system
            - name: tmp
              mountPath: /tmp
            - name: tmp
              mountPath: /opt/mastodon/tmp
          resources:
            {}
---
# Source: mastodon/templates/deployment-streaming.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mastodon-streaming
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: streaming
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: mastodon
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/component: streaming
  template:
    metadata:
      annotations:
        # roll the pods to pick up any db migrations or other changes
        checksum/config-secrets: "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b"
        checksum/config-configmap: "63b8ff4261fd1f4f3b79b90dbfa49e7f74dc30b0b06ddd2f8bf29c0cf9462f63"
      labels:
        app.kubernetes.io/name: mastodon
        app.kubernetes.io/instance: mastodon
        app.kubernetes.io/component: streaming
    spec:
      serviceAccountName: mastodon
      securityContext:
        fsGroup: 991
        runAsGroup: 991
        runAsNonRoot: true
        runAsUser: 991
        seccompProfile:
          type: RuntimeDefault
      containers:
        - name: mastodon-streaming
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            readOnlyRootFilesystem: true
          image: "ghcr.io/mastodon/mastodon-streaming:v4.3.4"
          imagePullPolicy: IfNotPresent
          command:
            - node
            - ./streaming
          envFrom:
            - configMapRef:
                name: mastodon-env
            - secretRef:
                name: mastodon
          env:
            - name: "DB_PASS"
              valueFrom:
                secretKeyRef:
                  name: database
                  key: password
            - name: "REDIS_PASSWORD"
              valueFrom:
                secretKeyRef:
                  name: redis
                  key: redis-password
            - name: "PORT"
              value: "4000"
          ports:
            - name: streaming
              containerPort: 4000
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /api/v1/streaming/health
              port: streaming
          readinessProbe:
            httpGet:
              path: /api/v1/streaming/health
              port: streaming
---
# Source: mastodon/templates/deployment-web.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mastodon-web
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: mastodon
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/component: web
      app.kubernetes.io/part-of: rails
  template:
    metadata:
      annotations:
        # roll the pods to pick up any db migrations or other changes
        checksum/config-secrets: "01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b"
        checksum/config-configmap: "63b8ff4261fd1f4f3b79b90dbfa49e7f74dc30b0b06ddd2f8bf29c0cf9462f63"
      labels:
        app.kubernetes.io/name: mastodon
        app.kubernetes.io/instance: mastodon
        app.kubernetes.io/component: web
        app.kubernetes.io/part-of: rails
    spec:
      serviceAccountName: mastodon
      securityContext:
        fsGroup: 991
        runAsGroup: 991
        runAsNonRoot: true
        runAsUser: 991
        seccompProfile:
          type: RuntimeDefault
      volumes:
        
        - name: assets
          persistentVolumeClaim:
            claimName: mastodon-assets
        - name: system
          persistentVolumeClaim:
            claimName: mastodon-system
        - name: tmp
          emptyDir:
            medium: Memory
      containers:
        - name: mastodon-web
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            readOnlyRootFilesystem: true
          image: "ghcr.io/mastodon/mastodon:v4.3.4"
          imagePullPolicy: IfNotPresent
          command:
            - bundle
            - exec
            - puma
            - -C
            - config/puma.rb
          envFrom:
            - configMapRef:
                name: mastodon-env
            - secretRef:
                name: mastodon
          env:
            - name: "DB_PASS"
              valueFrom:
                secretKeyRef:
                  name: database
                  key: password
            - name: "REDIS_PASSWORD"
              valueFrom:
                secretKeyRef:
                  name: redis
                  key: redis-password
            - name: "PORT"
              value: "3000"
          volumeMounts:
            
            - name: assets
              mountPath: /opt/mastodon/public/assets
            - name: system
              mountPath: /opt/mastodon/public/system
            - name: tmp
              mountPath: /tmp
          ports:
            - name: http
              containerPort: 3000
              protocol: TCP
          livenessProbe:
            tcpSocket:
              port: http
          readinessProbe:
            httpGet:
              path: /health
              port: http
          startupProbe:
            httpGet:
              path: /health
              port: http
            failureThreshold: 30
            periodSeconds: 5
---
# Source: mastodon/charts/redis/templates/master/application.yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mastodon-redis-master
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
    app.kubernetes.io/component: master
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/name: redis
      app.kubernetes.io/component: master
  serviceName: mastodon-redis-headless
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app.kubernetes.io/instance: mastodon
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/name: redis
        app.kubernetes.io/version: 7.2.4
        helm.sh/chart: redis-18.17.0
        app.kubernetes.io/component: master
      annotations:
        checksum/configmap: 86bcc953bb473748a3d3dc60b7c11f34e60c93519234d4c37f42e22ada559d47
        checksum/health: aff24913d801436ea469d8d374b2ddb3ec4c43ee7ab24663d5f8ff1a1b6991a9
        checksum/scripts: 380691285db1898b3f0464715b6da0babe4e94bcb358d29a371e4edcc635bfa4
        checksum/secret: 44136fa355b3678a1146ad16f7e8649e94fb4fc21fe77e8310c060f61caaff8a
    spec:
      
      securityContext:
        fsGroup: 1001
        fsGroupChangePolicy: Always
        supplementalGroups: []
        sysctls: []
      serviceAccountName: mastodon-redis-master
      automountServiceAccountToken: false
      affinity:
        podAffinity:
          
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/instance: mastodon
                    app.kubernetes.io/name: redis
                    app.kubernetes.io/component: master
                topologyKey: kubernetes.io/hostname
              weight: 1
        nodeAffinity:
          
      enableServiceLinks: true
      terminationGracePeriodSeconds: 30
      containers:
        - name: redis
          image: docker.io/bitnami/redis:7.2.4-debian-12-r9
          imagePullPolicy: "IfNotPresent"
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            readOnlyRootFilesystem: false
            runAsGroup: 0
            runAsNonRoot: true
            runAsUser: 1001
            seccompProfile:
              type: RuntimeDefault
          command:
            - /bin/bash
          args:
            - -c
            - /opt/bitnami/scripts/start-scripts/start-master.sh
          env:
            - name: BITNAMI_DEBUG
              value: "false"
            - name: REDIS_REPLICATION_MODE
              value: master
            - name: ALLOW_EMPTY_PASSWORD
              value: "no"
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: redis
                  key: redis-password
            - name: REDIS_TLS_ENABLED
              value: "no"
            - name: REDIS_PORT
              value: "6379"
          ports:
            - name: redis
              containerPort: 6379
          livenessProbe:
            initialDelaySeconds: 20
            periodSeconds: 5
            # One second longer than command timeout should prevent generation of zombie processes.
            timeoutSeconds: 6
            successThreshold: 1
            failureThreshold: 5
            exec:
              command:
                - sh
                - -c
                - /health/ping_liveness_local.sh 5
          readinessProbe:
            initialDelaySeconds: 20
            periodSeconds: 5
            timeoutSeconds: 2
            successThreshold: 1
            failureThreshold: 5
            exec:
              command:
                - sh
                - -c
                - /health/ping_readiness_local.sh 1
          volumeMounts:
            - name: start-scripts
              mountPath: /opt/bitnami/scripts/start-scripts
            - name: health
              mountPath: /health
            - name: redis-data
              mountPath: /data
            - name: config
              mountPath: /opt/bitnami/redis/mounted-etc
            - name: empty-dir
              mountPath: /opt/bitnami/redis/etc/
              subPath: app-conf-dir
            - name: empty-dir
              mountPath: /tmp
              subPath: tmp-dir
      volumes:
        - name: start-scripts
          configMap:
            name: mastodon-redis-scripts
            defaultMode: 0755
        - name: health
          configMap:
            name: mastodon-redis-health
            defaultMode: 0755
        - name: config
          configMap:
            name: mastodon-redis-configuration
        - name: empty-dir
          emptyDir: {}
        - name: redis-data
          emptyDir: {}
---
# Source: mastodon/charts/redis/templates/replicas/application.yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mastodon-redis-replicas
  namespace: "mastodon"
  labels:
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: redis
    app.kubernetes.io/version: 7.2.4
    helm.sh/chart: redis-18.17.0
    app.kubernetes.io/component: replica
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/instance: mastodon
      app.kubernetes.io/name: redis
      app.kubernetes.io/component: replica
  serviceName: mastodon-redis-headless
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app.kubernetes.io/instance: mastodon
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/name: redis
        app.kubernetes.io/version: 7.2.4
        helm.sh/chart: redis-18.17.0
        app.kubernetes.io/component: replica
      annotations:
        checksum/configmap: 86bcc953bb473748a3d3dc60b7c11f34e60c93519234d4c37f42e22ada559d47
        checksum/health: aff24913d801436ea469d8d374b2ddb3ec4c43ee7ab24663d5f8ff1a1b6991a9
        checksum/scripts: 380691285db1898b3f0464715b6da0babe4e94bcb358d29a371e4edcc635bfa4
        checksum/secret: 44136fa355b3678a1146ad16f7e8649e94fb4fc21fe77e8310c060f61caaff8a
    spec:
      
      securityContext:
        fsGroup: 1001
        fsGroupChangePolicy: Always
        supplementalGroups: []
        sysctls: []
      serviceAccountName: mastodon-redis-replica
      automountServiceAccountToken: false
      affinity:
        podAffinity:
          
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/instance: mastodon
                    app.kubernetes.io/name: redis
                    app.kubernetes.io/component: replica
                topologyKey: kubernetes.io/hostname
              weight: 1
        nodeAffinity:
          
      enableServiceLinks: true
      terminationGracePeriodSeconds: 30
      containers:
        - name: redis
          image: docker.io/bitnami/redis:7.2.4-debian-12-r9
          imagePullPolicy: "IfNotPresent"
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            readOnlyRootFilesystem: false
            runAsGroup: 0
            runAsNonRoot: true
            runAsUser: 1001
            seccompProfile:
              type: RuntimeDefault
          command:
            - /bin/bash
          args:
            - -c
            - /opt/bitnami/scripts/start-scripts/start-replica.sh
          env:
            - name: BITNAMI_DEBUG
              value: "false"
            - name: REDIS_REPLICATION_MODE
              value: replica
            - name: REDIS_MASTER_HOST
              value: mastodon-redis-master-0.mastodon-redis-headless.mastodon.svc.cluster.local
            - name: REDIS_MASTER_PORT_NUMBER
              value: "6379"
            - name: ALLOW_EMPTY_PASSWORD
              value: "no"
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: redis
                  key: redis-password
            - name: REDIS_MASTER_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: redis
                  key: redis-password
            - name: REDIS_TLS_ENABLED
              value: "no"
            - name: REDIS_PORT
              value: "6379"
          ports:
            - name: redis
              containerPort: 6379
          startupProbe:
            failureThreshold: 22
            initialDelaySeconds: 10
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 5
            tcpSocket:
              port: redis
          livenessProbe:
            initialDelaySeconds: 20
            periodSeconds: 5
            timeoutSeconds: 6
            successThreshold: 1
            failureThreshold: 5
            exec:
              command:
                - sh
                - -c
                - /health/ping_liveness_local_and_master.sh 5
          readinessProbe:
            initialDelaySeconds: 20
            periodSeconds: 5
            timeoutSeconds: 2
            successThreshold: 1
            failureThreshold: 5
            exec:
              command:
                - sh
                - -c
                - /health/ping_readiness_local_and_master.sh 1
          volumeMounts:
            - name: start-scripts
              mountPath: /opt/bitnami/scripts/start-scripts
            - name: health
              mountPath: /health
            - name: redis-data
              mountPath: /data
            - name: config
              mountPath: /opt/bitnami/redis/mounted-etc
            - name: empty-dir
              mountPath: /opt/bitnami/redis/etc
              subPath: app-conf-dir
            - name: empty-dir
              mountPath: /tmp
              subPath: tmp-dir
      volumes:
        - name: start-scripts
          configMap:
            name: mastodon-redis-scripts
            defaultMode: 0755
        - name: health
          configMap:
            name: mastodon-redis-health
            defaultMode: 0755
        - name: config
          configMap:
            name: mastodon-redis-configuration
        - name: empty-dir
          emptyDir: {}
        - name: redis-data
          emptyDir: {}
---
# Source: mastodon/templates/cronjob-media-remove.yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: mastodon-media-remove
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: media-remover
spec:
  schedule: 0 0 * * 0
  jobTemplate:
    spec:
      template:
        metadata:
          name: mastodon-media-remove
          labels:
            helm.sh/chart: mastodon-10.0.9
            app.kubernetes.io/name: mastodon
            app.kubernetes.io/instance: mastodon
            app.kubernetes.io/version: "v4.3.4"
            app.kubernetes.io/managed-by: Helm
            app.kubernetes.io/component: media-remover
        spec:
          restartPolicy: OnFailure
          # ensure we run on the same node as the other rails components; only
          # required when using PVCs that are ReadWriteOnce
          affinity:
            podAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
                - labelSelector:
                    matchExpressions:
                      - key: app.kubernetes.io/part-of
                        operator: In
                        values:
                          - rails
                  topologyKey: kubernetes.io/hostname
          securityContext:
            fsGroup: 991
            runAsGroup: 991
            runAsNonRoot: true
            runAsUser: 991
            seccompProfile:
              type: RuntimeDefault
          volumes:
            
            - name: assets
              persistentVolumeClaim:
                claimName: mastodon-assets
            - name: system
              persistentVolumeClaim:
                claimName: mastodon-system
          containers:
            - name: mastodon-media-remove
              image: "ghcr.io/mastodon/mastodon:v4.3.4"
              imagePullPolicy: IfNotPresent
              command:
                - bin/tootctl
                - media
                - remove
              envFrom:
                - configMapRef:
                    name: mastodon-env
                - secretRef:
                    name: mastodon
              env:
                - name: "DB_PASS"
                  valueFrom:
                    secretKeyRef:
                      name: database
                      key: password
                - name: "REDIS_PASSWORD"
                  valueFrom:
                    secretKeyRef:
                      name: redis
                      key: redis-password
                - name: "PORT"
                  value: "3000"
              securityContext:
                allowPrivilegeEscalation: false
                capabilities:
                  drop:
                  - ALL
              volumeMounts:
                
                - name: assets
                  mountPath: /opt/mastodon/public/assets
                - name: system
                  mountPath: /opt/mastodon/public/system
---
# Source: mastodon/templates/ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: mastodon
  labels:
    helm.sh/chart: mastodon-10.0.9
    app.kubernetes.io/name: mastodon
    app.kubernetes.io/instance: mastodon
    app.kubernetes.io/version: "v4.3.4"
    app.kubernetes.io/managed-by: Helm
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-production
    ingress.kubernetes.io/force-ssl-redirect: "true"
    projectcontour.io/websocket-routes: /api/v1/streaming/
spec:
  tls:
    - hosts:
        - "mastodon.davinkevin.fr"
      secretName: mastodon.davinkevin.fr
  rules:
    - host: "mastodon.davinkevin.fr"
      http:
        paths:
          - path: /
            backend:
              service:
                name: mastodon-web
                port:
                  number: 3000
            pathType: Prefix
          - path: /api/v1/streaming
            backend:
              service:
                name: mastodon-streaming
                port:
                  number: 4000
            pathType: Prefix

