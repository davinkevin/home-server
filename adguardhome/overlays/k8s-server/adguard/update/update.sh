#!/usr/bin/env bash

set -euo pipefail

cd $(mktemp -d)
scp adguard.davinkevin.lan:/opt/adguardhome/version.env . && source version.env
CURRENT_VERSION=$VERSION

source /job-config/version.env
NEW_VERSION=$VERSION

if [ "$CURRENT_VERSION" = "$NEW_VERSION" ]; then
  echo "Adguard version is up-to-date with version $CURRENT_VERSION"
  exit 0
fi

echo "New version has to be installed, old one is $CURRENT_VERSION and new is $NEW_VERSION"
scp /job-config/version.env adguard.davinkevin.lan:/opt/adguardhome/
ssh adguard.davinkevin.lan -t "sudo systemctl restart adguardhome.service"

